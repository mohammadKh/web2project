<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TestController extends Controller
{
    /**
     * @Route("/name/{name}", name="test_2")
     */
    public function testAction($name)
    {
        return $this->render('TestBundle:Test:test.html.twig', ['name'=>$name]);
    }
}
