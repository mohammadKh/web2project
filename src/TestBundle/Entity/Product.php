<?php
/**
 * Created by PhpStorm.
 * User: moha
 * Date: 6/20/19
 * Time: 10:39 AM
 */

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */

class Product {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, length=100)
     */
    private $name;
    private $price;
    private $description;

}