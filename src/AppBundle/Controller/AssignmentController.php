<?php
/**
 * Created by PhpStorm.
 * User: moha
 * Date: 5/25/19
 * Time: 12:57 AM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Entity\Product;

class AssignmentController extends Controller
{
    /**
     * @Route("/randNumber")
     * @throws \Exception
     */

    public function assignmentAction(){

        $number = random_int(1, 4);

        return $this->render('AppBundle:Assignment:assignment.html.twig', [
            'number' => $number
        ]);
    }
}