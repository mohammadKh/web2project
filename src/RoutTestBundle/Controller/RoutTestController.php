<?php

namespace RoutTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RoutTestController extends Controller
{
    /**
     * @Route("/test/{firstNum}/{operator}/{secondNum}")
     */
    public function parameterTestAction($firstNum, $operator, $secondNum)
    {
        $problem = false;
        if(is_numeric($firstNum))
            $firstNum = (int) $firstNum;
        else
            $problem = true;
        if(is_numeric($secondNum))
            $secondNum = (int) $secondNum;
        else
            $problem = true;

        return $this->render('RoutTestBundle:Default:index.html.twig',
            ['firstNum'=>$firstNum, 'operator'=>$operator, 'secondNum'=>$secondNum, 'problem'=>$problem]);
    }
}
